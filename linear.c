#include <stdio.h>
#include <bool.h>
#include <stdin.h>
#include <stdlib.h>


//Functions
//
//Create_Matrix
//Input: Num Rows, Num Columns
//Output: Reads numbers from input until matrix is filled and returns list of lists as matrix
int** create_Matrix(int rows, int cols){
	int **matrix = (int**) malloc(rows * sizeof(int*_));
	if (matrix == NULL){
		printf("Memory allocation error in create_matrix\n");
		exit(1);
	}

	for (int i = 0; i < rows; i++){
		matrix[i]= (int*)malloc(cols * sizeof(int));
		if (matrix[i] == NULL){
			printf("Memory allocation error in create_matrix\n");
			exit(1);
		}
	}
	return matrix;
}
//free matrix
//input: matrix array and num rows
//output: frees allocated memory, no return
void free_matrix(int** matrix, int rows){
	for (int i = 0; i < rows; i++){
		free(matrix[i]);
	}
	free(matrix);
}


//row sum
//input: matrix, row num
//output: integer sum of row
int row_sum(int** matrix, int row, int length){
	int sum=0;
	for (int i = 0; i < length; i++){
		sum+=matrix[row][i];
	}
	return sum;
}


//print matrix
void print_matrix(int** matrix, int rows, int cols){
	for (int i = 0; i < rows; i++){
		printf("|");
		for (int j = 0; j < cols; j++){
			printf(" %3d ", matrix[i][j]);
		}
		printf("|\n");
	}
}

//Main Program
int main(void){
	int m,n;
	printf("Enter # of rows in matrix: ");
	scanf("%d", m);
	printf("\nEnter # of columns in matrix: ");
	scanf("%d", n);
	int** matrix = create_Matrix(m, n);

}
